use modular::*;

fn main() {
    println!("\nModulo - field axioms example");
    println!("--");

    {
        let mod_num_1 = modulo!(23, 45);
        let mod_num_2 = modulo!(12, 45);
        let mod_num_3 = modulo!(2, 45);
        let sum_1 = mod_num_1 + (mod_num_2 + mod_num_3);
        let sum_2 = (mod_num_1 + mod_num_2) + mod_num_3;
        println!("\nAssociative addition:");
        println!(
            "{} + ({} + {}) = {}",
            mod_num_1, mod_num_2, mod_num_3, sum_1
        );
        println!(
            "({} + {}) + {} = {}",
            mod_num_1, mod_num_2, mod_num_3, sum_2
        );
    }

    {
        let mod_num_1 = modulo!(9, 128);
        let mod_num_2 = modulo!(14, 128);
        let mod_num_3 = modulo!(72, 128);
        let prod_1 = mod_num_1 * (mod_num_2 * mod_num_3);
        let prod_2 = (mod_num_1 * mod_num_2) * mod_num_3;
        println!("\nAssociative multiplication:");
        println!(
            "{} * ({} * {}) = {}",
            mod_num_1, mod_num_2, mod_num_3, prod_1
        );
        println!(
            "({} * {}) * {} = {}",
            mod_num_1, mod_num_2, mod_num_3, prod_2
        );
    }

    {
        let mod_num_1 = modulo!(23, 45);
        let mod_num_2 = modulo!(12, 45);
        let sum_1 = mod_num_1 + mod_num_2;
        let sum_2 = mod_num_2 + mod_num_1;
        println!("\nAssociative addition:");
        println!("{} + {} = {}", mod_num_1, mod_num_2, sum_1);
        println!("{} + {} = {}", mod_num_2, mod_num_1, sum_2);
    }

    {
        let mod_num_1 = modulo!(9, 77);
        let mod_num_2 = modulo!(14, 77);
        let prod_1 = mod_num_1 * mod_num_2;
        let prod_2 = mod_num_2 * mod_num_1;
        println!("\nAssociative multiplication:");
        println!("{} * {} = {}", mod_num_1, mod_num_2, prod_1);
        println!("{} * {} = {}", mod_num_2, mod_num_1, prod_2);
    }

    {
        let mod_num_1 = modulo!(23, 77);
        let mod_zero = Modulo::zero(77);
        let sum_1 = mod_num_1 + mod_zero;
        println!("\nAdditive identity:");
        println!("{} + {} = {}", mod_num_1, mod_zero, sum_1);
    }

    {
        let mod_num_1 = modulo!(128, 143);
        let mod_one = Modulo::one(143);
        let prod_1 = mod_num_1 * mod_one;
        println!("\nMultiplicative identity:");
        println!("{} * {} = {}", mod_num_1, mod_one, prod_1);
    }

    {
        let mod_num_1 = 5.to_modulo(12);
        let sum_1 = mod_num_1 + -mod_num_1;
        println!("\nAdditive inverse:");
        println!("{} + {} = {}", mod_num_1, -mod_num_1, sum_1);
    }

    {
        let mod_3 = 3.to_modulo(12);
        let mod_4 = 4.to_modulo(12);
        let mod_5 = 5.to_modulo(12);
        let dist_1 = mod_3 * (mod_4 + mod_5);
        let dist_2 = (mod_3 * mod_4) + (mod_3 * mod_5);
        println!("\nDistributive:");
        println!("{} * ({} + {}) = {}", mod_3, mod_4, mod_5, dist_1);
        println!(
            "({} * {}) + ({} * {}) = {}",
            mod_3, mod_4, mod_3, mod_5, dist_2
        );
    }
}
