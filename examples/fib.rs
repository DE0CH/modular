use modular::*;

struct Fib<T> {
    curr: T,
    next: T,
}

impl<T> Fib<T>
where
    T: Copy + Clone,
{
    fn new(curr: T, next: T) -> Fib<T> {
        Fib { curr, next }
    }
}

impl<T> Iterator for Fib<T>
where
    T: std::ops::Add<Output = T> + Copy + Clone,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        let curr = self.curr;
        self.curr = self.next;
        self.next = curr + self.next;
        Some(self.next)
    }
}

fn main() {
    println!("\nModulo -- fibonnacci example");
    println!("--");
    println!("\nLists the first 29 fibonnacci numbers using regular integers and");
    println!("numbers mod 1000\n");
    for fib in Fib::<u32>::new(0, 1)
        .zip(Fib::<Modulo>::new(0.to_modulo(1000), 1.to_modulo(1000)))
        .take(29)
    {
        let (nums, mods) = fib;
        println!("{} \t=> {}", nums, mods);
    }
}
