# Modular arithmetic

`edition: Rust-2018`

Modular arithmetic is a system of arithmetic for integers, where numbers "wrap around" upon reaching a certain value - which is known as the **modulus**. A common example would be values on a clock, which wrap around the modulus 12 (for a 12-hour clock)

For further information on modular arithmetic, see the [wikipedia](https://en.wikipedia.org/wiki/Modular_arithmetic) article on the same.

This crate allows for the creation and usage of such numbers.

# 1. Usage

To use this crate, import it from crates.io or github.com and use it as shown below.

### Cargo.toml

```toml
[dependencies]
modulo = "~1"
```

### main.rs

```rust
// Create a new modulo number from an integer, given a modulus
let mod_num = 76.to_modulo(7);
let mod_num2 = 45.to_modulo(16);

// This is equivalent to using the modulo! macro
let mod_mac = modulo!(76, 7);
let mod_mac2 = modulo!(78, 16);

// Check equality
assert!(mod_num == mod_mac);
assert!(mod_num2 != mod_mac2);

// Addition
assert_eq!(mod_num + mod_mac, modulo!(5, 7));

// Subtraction
assert_eq!(mod_mac2 - mod_num2, modulo!(0, 16));

// Multipication
assert_eq!(mod_num * mod_mac, modulo!(1, 7));

// Congruence
assert!(76.is_congruent(41, 7));
```

Further examples are given in the examples folder.

# 2. Installation

For development, you can fork this repo, or clone it directly from github/gitlab. The repo comes with examples of usage in the `/examples` directory.

```bash
$> cd <work_dir>
$> git clone git@gitlab.com:rust-algorithms/modular.git
$> cd modulo
$> cargo build && cargo test
$> cargo run --example fib
```

# 3. License

MIT
